Toptal Jogging App
==================

Demo: https://jogging.herokuapp.com/

Assignment
----------

Write an application that tracks jogging times of users.

- User must be able to create an account and log in
- When logged in, user can see, edit and delete his times he entered
- Each time entry when entered has a date, distance, and time
- When displayed, each time entry has an average speed
- Filter by dates from-to
- Report on average speed & distance per week
- REST API. Make it possible to perform all user actions via the API, including authentication
- All actions need to be done client side using AJAX, refreshing the page is not acceptable. (If a mobile app, disregard this)

Backend technologies
--------------------

- Python
- Django
- Django REST Framework
- ...

Frontend technologies
---------------------

- ReactJS
- Twitter Bootstrap
- Backbone (Router)
- jQuery (AJAX)
- Gulp  
- ...

REST API
--------

- [See documentation on Apiary.io](http://docs.joggingapi.apiary.io/)