
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token


class LoginTestCase(APITestCase):
    fixtures = ['api_test_data.json']
    login_url = '/api/token/'

    def test_login_success(self):
        response = self.client.post(self.login_url, {
            'username': 'john',
            'password': 'johnspassword',
        }, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data.get('token'))

    def test_login_invalid(self):
        response = self.client.post(self.login_url, {
            'username': 'john',
            'password': 'dummypassword',
        })

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class EntriesTestCase(APITestCase):
    fixtures = ['api_test_data.json']

    def setUp(self):
        token = Token.objects.get(user_id=1)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_list_entries(self):
        response = self.client.get('/api/entries/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertItemsEqual([e['id'] for e in response.data], [1, 2, 4])

    def test_add_entry_success(self):
        response = self.client.post('/api/entries/', {
            'distance_km': 4.25,
            'duration_min': 33,
            'date': '2015-01-15',
        }, 'json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue('id' in response.data)

        new_entry_id = response.data['id']

        response = self.client.get('/api/entries/')
        self.assertItemsEqual([e['id'] for e in response.data], [1, 2, 4,
                                                                 new_entry_id])
