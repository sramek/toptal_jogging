from django.conf.urls import patterns, include, url
from django.contrib import admin

from .views import AuthToken, EntryList, EntryDetail, UserList

urlpatterns = patterns('',
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns = patterns('',
    url(r'^token/', AuthToken.as_view()),
    url(r'^users/', UserList.as_view()),

    url(r'^entries/$', EntryList .as_view()),
    url(r'^entries/(?P<pk>\d+)/$', EntryDetail.as_view()),
)