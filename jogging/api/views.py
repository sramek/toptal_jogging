
from django.utils.decorators import method_decorator

from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status

from entries.models import Entry
from .serializers import EntrySerializer, UserSerializer


class UserList(generics.CreateAPIView):
    permission_classes = ()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


class AuthToken(ObtainAuthToken):

    @method_decorator(permission_classes((IsAuthenticated, )))
    def delete(self, request):
        Token.objects.filter(key=request.auth).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OwnedEntriesMixin(object):
    model = Entry

    def get_queryset(self):
        user = self.request.user
        return self.model.objects.filter(user=user)


class EntryList(OwnedEntriesMixin, generics.ListCreateAPIView):
    serializer_class = EntrySerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class EntryDetail(OwnedEntriesMixin, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EntrySerializer