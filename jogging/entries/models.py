
from django.conf import settings
from django.db import models


class Entry(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='entries',
                             db_index=True)
    date = models.DateField()
    distance_km = models.FloatField()
    duration_min = models.PositiveIntegerField()

    class Meta:
        ordering = ('-date',)
