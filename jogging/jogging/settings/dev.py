
from .base import *

DEBUG = True

TEMPLATE_DEBUG = True

SECRET_KEY = 'we78y$@gfq_on)ai536(xq#6f_=)f81927m+3kirgnox=ud#)n'

# SMTP server for local development
# Run it: python -m smtpd -n -c DebuggingServer localhost:1025
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025