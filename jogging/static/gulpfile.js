'use strict';

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var streamify = require('gulp-streamify');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');

var paths = {
    DEST_DIR: 'dist',

    LIB_JS_BUNDLE: 'libs.js',
    LIB_CSS_BUNDLE: 'libs.css',

    APP_JS_BUNDLE: 'app.js',
    APP_CSS_BUNDLE: 'app.css',

    APP_ENTRY_POINTS: ['./js/App.js'],
    APP_CSS: ['css/base.css']
};

var LIB_JS = [
    ['underscore', './bower_components/underscore/underscore.js'],
    ['backbone', './bower_components/backbone/backbone.js'],
    ['react', './bower_components/react/react.min.js'],
    ['moment', './bower_components/moment/moment.js'],
    ['jquery', './bower_components/jquery/dist/jquery.js'],
    ['pickadate', './bower_components/pickadate/lib/picker.js'],
    ['pickadate.date', './bower_components/pickadate/lib/picker.date.js']
];

var LIB_CSS = [
    'bower_components/bootstrap/dist/css/bootstrap.min.css',
    'bower_components/pickadate/lib/compressed/themes/default.css',
    'bower_components/pickadate/lib/compressed/themes/default.date.css'
];

gulp.task('browser-sync', function() {
    browserSync({
        proxy: 'localhost:8000'
    });
});

gulp.task('libs', function() {
    var bundler = browserify({
        debug: false,  // Don't provide source maps for vendor libs
    });

    LIB_JS.forEach(function(lib) {
        bundler.require(lib[1], {expose: lib[0]});
    });

    bundler.bundle()
        .pipe(source(paths.LIB_JS_BUNDLE))
        .pipe(streamify(uglify(paths.LIB_JS_BUNDLE)))
        .pipe(gulp.dest(paths.DEST_DIR));

    gulp.src(LIB_CSS)
        .pipe(concat(paths.LIB_CSS_BUNDLE))
        .pipe(gulp.dest(paths.DEST_DIR + '/css'));

    gulp.src(paths.APP_CSS)
        .pipe(concat(paths.APP_CSS_BUNDLE))
        .pipe(gulp.dest(paths.DEST_DIR + '/css'));
});

gulp.task('watch', ['browser-sync'], function() {
    var bundler = browserify({
        entries: paths.APP_ENTRY_POINTS,
        transform: [reactify],
        debug: true,
        cache: {}, packageCache: {}, fullPaths: true
    });

    bundler.external(LIB_JS.map(function(lib) {
        return lib[0];
    }));

    var watcher = watchify(bundler);

    return watcher.on('update', function() {
        watcher.bundle()
            .pipe(source(paths.APP_JS_BUNDLE))
            .pipe(gulp.dest(paths.DEST_DIR));

        browserSync.reload();
        console.log('Updated');
    })
        .bundle()
        .pipe(source(paths.APP_JS_BUNDLE))
        .pipe(gulp.dest(paths.DEST_DIR));
});

gulp.task('default', ['watch']);

gulp.task('build', ['libs'], function() {
    browserify({
        entries: paths.APP_ENTRY_POINTS,
        transform: [reactify]
    }).external(LIB_JS.map(function(lib) {
        return lib[0];
    }))
        .bundle()
        .pipe(source(paths.APP_JS_BUNDLE))
        .pipe(streamify(uglify(paths.APP_JS_BUNDLE)))
        .pipe(gulp.dest(paths.DEST_DIR));
});
