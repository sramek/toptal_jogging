'use strict';

var React = require('react');
var jquery = require('jquery');
var Backbone = require('backbone');
var AuthForms = require('./AuthForms');
var HomeScreen = require('./HomeScreen');
var JoggingAPI = require('./JoggingAPI');

Backbone.$ = jquery;

var Header = React.createClass({
    render: function() {
        return (
            <div className="header">
                <nav>
                    <ul className="nav nav-pills pull-right">{this.props.children}</ul>
                </nav>
                <h3 className="text-muted">Jogging <small>{this.props.user}</small>
                </h3>
            </div>
        );
    }
});


var Footer = React.createClass({
    render: function() {
        return (
            <footer className="text-center">
                <p>&copy; 2015, created by <a href="http://osramek.github.io" target="_blank">osramek</a></p>
            </footer>
        )
    }
});


var JoggingApp = React.createClass({
    LOGIN_SCREEN: 1,
    SIGNUP_SCREEN: 2,
    HOME_SCREEN: 3,

    getInitialState: function() {
        return {
            content: this.getIndexScreen()
        }
    },

    componentDidMount: function() {
        var that = this;
        var Router = Backbone.Router.extend({
            routes: {
                '': 'index',
                'login': 'login'
            },
            index: function() {
                that.setState({content: that.getIndexScreen()});
            },

            login: function() {
                that.setState({content: that.LOGIN_SCREEN});
            }
        });

        JoggingAPI.init();

        this.router = new Router();
        Backbone.history.start();
    },

    getIndexScreen: function() {
        return (JoggingAPI.getAuthToken()) ? this.HOME_SCREEN : this.SIGNUP_SCREEN;
    },

    onLogin: function() {
        this.router.navigate('', {trigger: true});
    },

    onSignUp: function() {
        // WARNING: router.navigate will not work in this case because route is not changing
        this.setState({content: this.HOME_SCREEN});
    },

    doLogout: function() {
        JoggingAPI.logoutUser();
        this.router.navigate('login', {trigger: true});
    },

    render: function() {
        var content;
        var headerLinks;
        var home;   //TODO: find the problem with using headerLinks
        switch (this.state.content) {
            case this.LOGIN_SCREEN:
                content = (<AuthForms.SignInForm onLogin={this.onLogin} />);
                home = <li><a href="#"><span className="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a></li>
                break;
            case this.SIGNUP_SCREEN:
                content = (<AuthForms.SignUpForm onSignUp={this.onSignUp} />);
                headerLinks = (<li><a href="#login"><span className="glyphicon glyphicon-log-in" aria-hidden="true"></span> Sign in</a></li>);
                break;
            case this.HOME_SCREEN:
                content = (<HomeScreen />);
                headerLinks = (<li><a href="javascript:;" onClick={this.doLogout}><span className="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log out</a></li>);
                break;
        }

        return (
            <div>
                <Header>{home}{headerLinks}</Header>
                {content}
                <Footer />
            </div>
        );
    }
});

React.render(
    <JoggingApp />,
    document.getElementById('content')
);

module.exports = JoggingApp;