'use strict';

var React = require('react');
var _ = require('underscore');

var JoggingAPI = require('./JoggingAPI');
var Utils = require('./Utils');


var AuthFormMixin = _.extend({}, Utils.FormErrorMixin, {
    getUsername: function() {
        return this.refs.username.getDOMNode().value;
    },

    getPassword: function() {
        return this.refs.password.getDOMNode().value;
    },

    render: function() {
        var tagline;
        if (this.state.error) {
            tagline = (<p className="text-danger">{this.state.error}</p>);
        } else {
            tagline = this.props.tagline;
        }

        return (
            <div className="jumbotron">
                <h1>Jogging</h1>
                <p>{tagline}</p>

                <form className="form-signin" onSubmit={this.handleSubmit}>
                    <input type="text" className="form-control" placeholder="Username" ref="username" autoFocus={true} />
                    <input type="password" className="form-control" placeholder="Password" ref="password" />
                    <button className="btn btn-lg btn-primary btn-block" type="submit">{this.props.submitText}</button>
                </form>
            </div>
        );
    }
});


var SignInForm = React.createClass({
    mixins: [AuthFormMixin],

    getDefaultProps: function() {
        return {
            submitText: 'Sign in',
            tagline: 'Welcome back. Please, sign in.'
        }
    },

    handleSubmit: function(e) {
        e.preventDefault();

        if (!this.getUsername() || !this.getPassword()) {
            this.showFormError('Please fill your credentials.');
            return;
        }

        var loginRequest = JoggingAPI.loginUser(this.getUsername(), this.getPassword());
        loginRequest
            .done(this.props.onLogin.bind(this))
            .fail(function() {
                this.showFormError('Incorrect username or password!');
            }.bind(this));
    }
});


var SignUpForm = React.createClass({
    mixins: [AuthFormMixin],

    getDefaultProps: function() {
        return {
            submitText: 'Create new account',
            tagline: 'Minimalistic app to keep track of your jogging.'
        }
    },

    handleSubmit: function(e) {
        e.preventDefault();

        if (!this.getUsername() || !this.getPassword()) {
            this.showFormError('Please fill both fields.');
            return;
        }

        var signUpRequest = JoggingAPI.createUser(this.getUsername(), this.getPassword());
        signUpRequest
            .done(this.props.onSignUp.bind(this))
            .fail(function() {
                this.showFormError('This username is already taken.');
            }.bind(this));
    }
});

module.exports = {
    SignInForm: SignInForm,
    SignUpForm: SignUpForm
};