'use strict';

var React = require('react');
var moment = require('moment');

var EntryLine = React.createClass({
    handleEditClick: function(fieldName) {
        this.props.onStartEdit(fieldName);
    },

    handleDeleteClick: function(e) {
        e.preventDefault();
        this.props.onDelete(this.props.entry);
    },

    render: function() {
        var entry = this.props.entry;
        var avgSpeed = entry.distance_km / (entry.duration_min / 60);
        var dateFormat = (this.props.showYear) ? 'MMM Do[,] YYYY' : 'MMM Do';
        return (
            <tr>
                <th scope="row">{moment(this.props.entry.date).format('dddd')}<br/>{moment(this.props.entry.date).format(dateFormat)}</th>
                <td>
                    <a href="javascript:;" className="editable" onClick={this.handleEditClick.bind(this, 'distance')}>{Math.round(100 * this.props.entry.distance_km) / 100} km</a>
                </td>
                <td>
                    <a href="javascript:;" className="editable" onClick={this.handleEditClick.bind(this, 'duration')}>{this.props.entry.duration_min} min</a>
                </td>
                <td>
                    <button className="btn btn-danger delete" onClick={this.handleDeleteClick}>
                        <span className="glyphicon glyphicon-trash"></span>
                    </button>
                    <span className="text-muted">{avgSpeed.toFixed(1)} km/h</span>
                </td>
            </tr>
        );
    }
});


var EditingEntryLine = React.createClass({
    getInitialState: function() {
        return {
            distance: this.props.entry.distance_km,
            duration: this.props.entry.duration_min
        }
    },

    handleFieldChange: function(e) {
        var newState = {};
        var field = e.target.getAttribute('id');
        newState[field] = e.target.value;
        this.setState(newState);
    },

    componentDidMount: function() {
        if (this.props.editingField) {
            this.refs[this.props.editingField].getDOMNode().focus();
        }
    },

    saveForm: function() {
        var entry = this.props.entry;
        var entryData = {
            date: moment(entry.date).hours(0).minutes(0).seconds(0),
            distance_km: this.state.distance,
            duration_min: this.state.duration
        }
        this.props.onEdit(entryData);
    },

    handleSubmit: function(e) {
        e.preventDefault();
        this.saveForm()
    },

    handleCancel: function(e) {
        e.preventDefault();
        this.props.onCancelEdit();
    },

    handleDeleteClick: function(e) {
        e.preventDefault();
        this.props.onDelete(this.props.entry);
    },

    render: function() {
        return (
            <tr className="editing">
                <th scope="row">{moment(this.props.entry.date).format('dddd')}<br/>{moment(this.props.entry.date).format('MMM Do')}</th>
                <td>
                    <form id="entryform" onSubmit={this.handleSubmit}></form>
                    <input type="text" className="form-control inplace" onChange={this.handleFieldChange} value={this.state.distance} id="distance" ref="distance" form="entryform" />
                    km</td>
                <td>
                    <input type="text" className="form-control inplace" onChange={this.handleFieldChange} value={this.state.duration} id="duration" ref="duration" form="entryform" />
                    min</td>
                <td>
                    <button type="submit" className="btn btn-success" form="entryform"><span className="glyphicon glyphicon-ok"></span></button>
                    <button className="btn btn-warning" onClick={this.handleCancel}><span className="glyphicon glyphicon-remove"></span></button>
                    <button className="btn btn-danger delete" onClick={this.handleDeleteClick}>
                        <span className="glyphicon glyphicon-trash"></span>
                    </button>
                </td>
            </tr>
        );
    }
});


var EntryTable = React.createClass({
    render: function() {
        var props = this.props;

        if (!props.entries.length) {
            return (<div className="alert alert-warning">No entries in the given period.</div>);
        }

        var showYear = props.entries[0].date.year() != props.entries[props.entries.length - 1].date.year();

        var lines = props.entries.map(function(entry, index) {
            if (index == props.editingIndex) {
                return (<EditingEntryLine entry={entry} key={index} editingField={props.editingField} onCancelEdit={props.onCancelEdit} onEdit={props.onEdit.bind(null, index)} onDelete={props.onDelete}></EditingEntryLine>);
            }
            return (
                <EntryLine entry={entry} key={index} showYear={showYear} onStartEdit={props.onStartEdit.bind(null, index)} onDelete={props.onDelete}></EntryLine>
            );
        }.bind(this));

        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Distance</th>
                        <th>Duration</th>
                        <th>Avg. Speed</th>
                    </tr>
                </thead>
                <tbody>{lines}</tbody>
            </table>
        );
    }
});

module.exports = EntryTable;