'use strict';

var React = require('react');
var $ = require('jquery');
var _ = require('underscore');
var moment = require('moment');

require('pickadate');
require('pickadate.date');

var JoggingAPI = require('./JoggingAPI');
var EntryTable = require('./EntryTable');
var WeekReport = require('./WeekReport');
var Utils = require('./Utils');


var AddEntryForm = React.createClass({
    mixins: [Utils.FormErrorMixin],

    getInitialState: function() {
        return {
            distance: '',
            duration: ''
        }
    },

    componentDidMount: function() {
        var picker = $(this.refs.datepicker.getDOMNode()).pickadate({
            clear: null,
            max: true  // today is maximum
        });

        picker.pickadate('picker').set('select', new Date());
    },

    handleSubmit: function(e) {
        e.preventDefault();

        var date = $(this.refs.datepicker.getDOMNode()).pickadate('picker').get('select').obj;
        var error = this.validate();
        if (error) {
            this.showFormError(error);
            return;
        }

        this.props.onAdd({
            date: moment(date).hours(0).minutes(0).seconds(0),
            distance_km: this.state.distance,
            duration_min: this.state.duration
        });

        this.setState({
            error: null,
            distance: '',
            duration: ''
        }, function() {
            this.refs.distance.getDOMNode().focus();
        }.bind(this));
    },

    handleFieldChange: function(e) {
        var newState = {};
        var field = e.target.getAttribute('id');
        newState[field] = e.target.value;
        this.setState(newState);
    },

    validate: function() {
        if (!this.allFieldsFilled()) {
            return 'Please, fill all the fields.';
        }
        if (!/^[1-9]\d*(\.\d{1,2})?$/.test(this.state.distance)) {
            return 'Distance has to be decimal number.';
        }
        if (!/^\d+$/.test(this.state.duration)) {
            return 'Duration has to be whole number.';
        }
    },

    allFieldsFilled: function() {
        return !!(this.state.duration && this.state.distance);
    },

    render: function() {
        var errorMsg;
        if (this.state.error) {
            errorMsg = (<div className="col-xs-12"><p className="text-danger">{this.state.error}</p></div>);
        }
        var isSubmitAllowed = this.allFieldsFilled();

        return (
            <form onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-xs-12 col-sm-4 form-group">
                        <label className="control-label" htmlFor="add-datepicker">Date</label>
                        <input type="text" id="add-datepicker" className="form-control datepicker" ref="datepicker" />
                    </div>
                    <div className="col-xs-12 col-sm-4 form-group">
                        <label className="control-label" htmlFor="distance">Distance</label>
                        <div className="input-group">
                            <input type="text" id="distance" className="form-control" value={this.state.distance} onChange={this.handleFieldChange} ref="distance" />
                            <span className="input-group-addon">km</span>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-4 ">
                        <label className="control-label" htmlFor="duration">Duration</label>
                        <div className="input-group form-group">
                            <input type="text" id="duration" className="form-control" value={this.state.duration} onChange={this.handleFieldChange} ref="duration" />
                            <span className="input-group-addon">min</span>
                        </div>
                    </div>
                    {errorMsg}
                    <div className="col-xs-12">
                        <button type="submit" className="btn btn-primary btn-lg btn-block" disabled={!isSubmitAllowed}>Add new entry</button>
                    </div>
                </div>
            </form>
        );
    }
});


var DateField = React.createClass({
    getInitialState: function() {
        return {date: null};
    },

    componentDidMount: function() {
        var picker = $(this.refs.datepicker.getDOMNode()).pickadate({
            max: true,  // today is maximum 
            onSet: function(value) {
                var date = null;

                if (value.select) {
                    //WARNING: value.select differs depending wheather value was selected by mouse or by keyboard !?
                    date = moment(new Date((value.select.pick) ? value.select.pick : value.select));
                }
                this.setState({date: date});
                this.props.onChange(date);
            }.bind(this)
        });

        picker = picker.pickadate('picker');
    },

    render: function() {
        return (
            <input type="text" id={this.props.htmlId} className="form-control datepicker" ref="datepicker" />
        );
    }
});


var Navigation = React.createClass({
    getInitialState: function() {
        return {
            filterKey: 'last4weeks',
            customFromDate: null,
            customToDate: null
        };
    },

    statics: {
        getFirstDay4weekAgo: function() {
            return moment().subtract(21 + moment().weekday(), 'days').hours(0).minutes(0).seconds(0)
        },
    },

    dateFilters: [
        {key: 'last4weeks', label: 'last 4 weeks'},
        {key: 'custom', label: 'custom range'},
        {key: 'all', label: 'all'}
    ],

    handleDateFilterChange: function(e) {
        var filterKey = e.target.value;
        this.setState({filterKey: filterKey});
        this.triggerChange(this.props.activeTabKey, filterKey);
    },

    triggerChange: function(tabKey, filterKey) {
        switch (filterKey) {
            case 'last4weeks':
                var fromDate = Navigation.getFirstDay4weekAgo();
                this.props.onChange(tabKey, fromDate, null);
                break;
            case 'custom':
                this.props.onChange(tabKey, this.state.customFromDate, this.state.customToDate);
                break;
            case 'all':
                this.props.onChange(tabKey, null, null);
                break;
        }
    },

    handleCustomFromDateChange: function(date) {
        this.setState({customFromDate: date});
        this.props.onChange(this.props.activeTabKey, date, this.state.customToDate);
    },

    handleCustomToDateChange: function(date) {
        this.setState({customToDate: date});
        this.props.onChange(this.props.activeTabKey, this.state.customFromDate, date);
    },

    render: function() {
        var props = this.props;
        var tabs = props.tabs.map(function(tab, i) {
                return (
                    <li role="presentation" key={i} className={ (tab.key === props.activeTabKey) ? 'active' : ''}>
                        <a href="javascript:;" onClick={this.triggerChange.bind(this, tab.key, this.state.filterKey)}>{tab.label}</a>
                    </li>);
            }.bind(this));

        var dateFilters = this.dateFilters.map(function(filter, i) {
            return (<option value={filter.key} key={i}>{filter.label}</option>);
        });

        var content;

        if (this.state.filterKey == 'custom') {
            content = (
                <form className="form-inline well" id="customRangeForm">
                    <div className="form-group">
                        <label className="control-label" htmlFor="fromDate">From:</label>
                        <DateField htmlId="fromDate" onChange={this.handleCustomFromDateChange} />
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="toDate">To:</label>
                        <DateField htmlId="toDate" onChange={this.handleCustomToDateChange} />
                    </div>
                </form>);
        }

        return (
            <div id="homeTabs">
                <ul className="nav nav-tabs">
                    {tabs}
                    <li id="filterTab">
                        <form>
                            <select className="form-control" onChange={this.handleDateFilterChange} value={this.state.filterKey}>
                                {dateFilters}
                            </select>
                        </form>
                    </li>
                </ul>
                {content}
            </div>
        );
    }
});


var HomeScreen = React.createClass({
    navigationTabs: [
        {key: 'list', label: 'Your entries'},
        {key: 'report', label: 'Weekly report'}
    ],

    getInitialState: function() {
        return {
            entries: [],
            editingIndex: null,
            activeTabKey: 'list',
            dateFrom: Navigation.getFirstDay4weekAgo(),
            dateTo: null
        }
    },

    componentDidMount: function() {
        JoggingAPI.listEntries().done(function(data) {
            this.setState({entries: data});
        }.bind(this));
    },

    addEntry: function(entryData) {
        this.cancelEditingEntry();
        JoggingAPI.addEntry(entryData)
            .done(this._addEntry.bind(this));
    },

    _addEntry: function(entry) {
        // entries are ordered by date (descending) - find proper position for new entry
        var entries = this.state.entries;
        for (var i = 0; i < entries.length; i++) {
            if (entries[i].date.valueOf() <= entry.date.valueOf()) {
                entries.splice(i, 0, entry);
                this.setState({entries: entries});
                return;
            }
        }

        entries.push(entry);
        this.setState({entries: entries});
    },

    deleteEntry: function(entry) {
        this.cancelEditingEntry();
        JoggingAPI.deleteEntry(entry.id);
        this.setState({
            entries: _.reject(this.state.entries, function(e) {
                return e.id === entry.id
            })
        });
    },

    startEditingEntry: function(index, focus) {
        this.setState({editingIndex: index, editingField: focus});
    },

    cancelEditingEntry: function() {
        this.setState({editingIndex: null});
    },

    updateEntry: function(index, entryData) {
        var entry = this.state.entries[index];
        JoggingAPI.updateEntry(entry.id, entryData)
            .done(function(data) {
                _.extend(entry, data);

                this.setState({
                    entries: this.state.entries,
                    editingIndex: (this.state.editingIndex === index) ? null : this.state.editingIndex
                });
            }.bind(this))
            .fail(function(jqXHR, textStatus, error) {
                if (jqXHR.status === 404) {
                    alert('Entry was just deleted in another browser.');
                    this.deleteEntry(entry);
                }
            }.bind(this));
    },

    handleNavigationChange: function(tabKey, dateFrom, dateTo) {
        this.setState({
            activeTabKey: tabKey,
            dateFrom: dateFrom,
            dateTo: dateTo
        });
    },

    filterEntries: function(entries, dateFrom, dateTo) {
        if (!dateFrom && !dateTo) {
            return entries;
        }

        return _.filter(entries, function(e) {
            return (!dateFrom || (e.date.valueOf() >= dateFrom.valueOf())) && (!dateTo || (e.date.valueOf() <= dateTo.valueOf()));
        });
    },

    render: function() {
        var content;
        var entries = this.filterEntries(this.state.entries, this.state.dateFrom, this.state.dateTo);

        switch (this.state.activeTabKey) {
            case 'list':
                content = <EntryTable entries={entries} editingIndex={this.state.editingIndex} editingField={this.state.editingField} onDelete={this.deleteEntry} onStartEdit={this.startEditingEntry} onCancelEdit={this.cancelEditingEntry} onEdit={this.updateEntry} />;
                break;
            case 'report':
                var minDate = (this.state.entries.length) ? this.state.entries[this.state.entries.length - 1].date : null;
                content = <WeekReport entries={entries} dateFrom={this.state.dateFrom} dateTo={this.state.dateTo} minDate={minDate} />;
                break;
        }

        return (
            <div>
                <AddEntryForm onAdd={this.addEntry} tabs={this.navigationTabs} />
                <Navigation tabs={this.navigationTabs} activeTabKey={this.state.activeTabKey} onChange={this.handleNavigationChange} />
                {content}
            </div>
        );
    }
});

module.exports = HomeScreen;