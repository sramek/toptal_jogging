'use strict';

var $ = require('jquery');
var _ = require('underscore');
var moment = require('moment');

var JoggingAPI = {
    DATE_FORMAT: 'YYYY-MM-DD',

    getAuthToken: function() {
        return window.localStorage.getItem('token');
    },

    setAuthToken: function(token) {
        window.localStorage.setItem('token', token);
    },

    removeAuthToken: function() {
        window.localStorage.removeItem('token');
    },

    init: function() {
    	$.ajaxSetup({
            dataType: 'json',
            statusCode: {
                '401': function() {
                    alert('Login session expired.');
                    this.removeAuthToken();
                    window.location.replace('#login');
                }.bind(this)
            }
        });
    },

    getAuthHeaders: function() {
        return {'Authorization': 'Token ' + this.getAuthToken()};
    },

    loginUser: function(username, password) {
        return $.ajax({
            url: '/api/token/',
            type: 'POST',
            data: {
                username: username, password: password
            }, success: function(data) {
                this.setAuthToken(data.token);
            }.bind(this)
        });
    },

    createUser: function(username, password) {
        return $.ajax({
            url: '/api/users/',
            type: 'POST',
            data: {
                username: username,
                password: password
            }, success: function(data) {
                this.setAuthToken(data.token);
            }.bind(this)
        });
    },

    logoutUser: function() {
        var headers = this.getAuthHeaders()
        this.removeAuthToken();
        return $.ajax({
            url: '/api/token/',
            type: 'DELETE',
            headers: headers
        });
    },

    addEntry: function(entryData) {
        var deferred = $.Deferred();
        $.ajax({
            url: '/api/entries/',
            type: 'POST',
            dataType: 'json',
            data: this._serializeEntry(entryData),
            headers: this.getAuthHeaders(),
            success: function(data) {
                deferred.resolve(this._parseEntry(data));
            }.bind(this),
            error: deferred.reject
        });
        return deferred.promise();
    },

    listEntries: function() {
        var that = this;
        var deferred = $.Deferred();
        $.ajax({
            url: '/api/entries/',
            headers: this.getAuthHeaders(),
            success: function(data) {
                $.each(data, function(index, entry) {
                    that._parseEntry(entry);
                });
                deferred.resolve(data);
            },
            error: deferred.reject
        });
        return deferred.promise();
    },

    updateEntry: function(entryId, entryData) {
        var deferred = $.Deferred();
        $.ajax({
            url: '/api/entries/' + entryId + '/',
            type: 'PUT',
            data: this._serializeEntry(entryData),
            headers: this.getAuthHeaders(),
            success: function(data) {
                deferred.resolve(this._parseEntry(data));
            }.bind(this),
            error: deferred.reject
        });
        return deferred.promise();
    },

    deleteEntry: function(entryId) {
        return $.ajax({
            url: '/api/entries/' + entryId + '/',
            type: 'DELETE',
            headers: this.getAuthHeaders()
        });
    },

    _serializeEntry: function(entry) {
        var data = _.clone(entry);
        data.date = entry.date.format(this.DATE_FORMAT);
        return data;
    },

    _parseEntry: function(entry) {
        entry.date = moment(entry.date, this.DATE_FORMAT).hours(0).minutes(0).seconds(0);
        return entry;
    }
};

module.exports = JoggingAPI;