'use strict';

var FormErrorMixin = {
    SHOW_ERROR_SEC: 3,

    getInitialState: function() {
        return {}
    },

    showFormError: function(error) {
        this.setState({error: error});
        setTimeout(function() {
            this.setState({error: null});
        }.bind(this), this.SHOW_ERROR_SEC * 1000);
    }
};

module.exports = {
    FormErrorMixin: FormErrorMixin
};