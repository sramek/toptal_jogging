'use strict';

var React = require('react');
var _ = require('underscore');
var moment = require('moment');

var WeekReport = React.createClass({
    buildReportLines: function(entries, fromDate, toDate) {
        if (!fromDate) {
            fromDate = entries[entries.length - 1].date;
        }
        var weekEntries = [];
        var weekFrom = this._makeFirstDayOfWeek(fromDate);
        var weekTo = weekFrom.clone().add(6, 'days');
        var i = entries.length - 1;
        var weekNumber = weekFrom.diff(this._makeFirstDayOfWeek(this.props.minDate), 'weeks') + 1;

        var lines = [];

        while (true) {
            while (i >= 0 && entries[i].date.valueOf() <= weekTo.valueOf()) {
                weekEntries.push(entries[i]);
                i--;
            }

            var weekEntriesCnt = weekEntries.length;
            var line = {
                number: weekNumber,
                from: weekFrom.clone(),
                to: weekTo.clone(),
                avgDistance: null,
                avgSpeed: null
            };

            if (weekEntriesCnt) {
                var sumDistance = _.reduce(_.pluck(weekEntries, 'distance_km'), function(a, b) { return a + b; }, 0);
                var sumDuration = _.reduce(_.pluck(weekEntries, 'duration_min'), function(a, b) { return a + b; }, 0);

                line.avgDistance = sumDistance / weekEntriesCnt;
                line.avgSpeed = sumDistance / (sumDuration / 60);
            }

            lines.unshift(line);

            weekEntries = [];
            weekFrom.add(7, 'days');
            weekTo.add(7, 'days').hours(0).minutes(0).seconds(0);
            weekNumber++;

            if ((!toDate && i < 0) || (toDate && weekFrom.valueOf() > toDate.valueOf()) || (weekNumber > 1000)) {
                // weeekNumber >= 1000 just to make sure we not stuck in infinite loop if something went wrong
                break;
            }
        }

        return lines;
    },

    _makeFirstDayOfWeek: function(date) {
        return moment(date).subtract(moment(date).weekday(), 'days');
    },

    render: function() {
        var entries = this.props.entries;
        if (!entries.length) {
            return (<div className="alert alert-warning">No entries in the given period.</div>);
        }

        var linesData = this.buildReportLines(this.props.entries, this.props.dateFrom, this.props.dateTo);
        var lines = linesData.map(function(line) {
            return (
                <tr>
                    <th scope="row">Week #{line.number}<br/>{line.from.format('MMM Do')} - {line.to.format('MMM Do')}</th>
                    <td>{(line.avgDistance != null) ? line.avgDistance.toFixed(1) + ' km' : '-' }</td>
                    <td>{(line.avgSpeed != null) ? line.avgSpeed.toFixed(1) + ' km/h' : '-' }</td>
                </tr>
            );
        });

        return (
            <table className="table table-stripped">
                <thead>
                    <tr>
                        <th>Week</th>
                        <th>Avg. Distance</th>
                        <th>Avg. Speed</th>
                    </tr>
                </thead>
                <tbody>{lines}</tbody>
            </table>
        );
    }
});

module.exports = WeekReport;